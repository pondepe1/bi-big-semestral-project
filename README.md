# BI-BIG Semestrální práce

## Business pohled na data

ABV: Alcohol By Volume

## Spuštění systému

    docker kill $(docker ps -q)
    docker rm $(docker ps -a -q)

## Import datasetů do HDFS

Informační stránka HDFS úložiště je dostupná na adrese:  
http://<IP adresa>:50070

IP adresu lze zjistit příkazem:

    ifconfig

Programy potřebné k práci s HDFS, pro nás je to hlavně utilita hdfs jsou v adresáři /usr/local/hadoop/bin/. Pro pohodlnou práci si tuto cestu přidáme do bash proměnné PATH:

    export PATH=$PATH:/usr/local/hadoop/bin/

Nyní musíme do Hadoop Containeru stáhnout datasety. Následně na HDFS vytvoříme adresář /semestral_project a stažené datasety do něj umístíme.

    curl https://gitlab.fit.cvut.cz/pondepe1/bi-big/raw/master/SemestralProject/datasets/datasets.tar.gz --output datasets.tar.gz
    tar -xvf datasets.tar.gz

    hdfs dfs -mkdir /semestral_project
    hdfs dfs -put ./breweries.csv /semestral_project/breweries.csv
    hdfs dfs -put ./beers.csv /semestral_project/beers.csv
    hdfs dfs -put ./reviews.csv /semestral_project/reviews.csv

## Apache Spark: Agragace nad datasety

### Teorie

Datasety ve formátu CSV umístit na HDFS.

Jako příklad vezměme ukázku z konce 5. cvičení.

Dataset lze z HDFS importovat do Apache Spark.

    val people = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load("hdfs://172.17.0.5:9000/test/data.csv")

Nyní jsme vytvořili Spark DataFrame. DataFrame můžeme registrovat jako Temporary Table.

    people.registerTempTable("peopletab")

Nad Temporary Table se lze dotazovat pomocí jazyka SQL. Lze v něm tudíž provádět také agregace.

    spark.sqlContext.sql("select gender, count(id) from peopletab group by gender").show()

V případě vynechání metody show() vrací příkaz nový DataFrame. Ten lze uložit do proměnné.

    val aggregated = spark.sqlContext.sql("select gender, count(id) from peopletab group by gender")

Nově vytvořený DataFrame lze exportovat ze Sparku na HDFS a vytvořit tak nový dataset pomocé příkazu:

    aggregated.write.csv("hdfs://172.17.0.5:9000/test/aggregated.csv")

### Praxe

#### Agregace 1

    val breweries = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load("hdfs://172.17.0.5:9000/semestral_project/breweries.csv")

    breweries.registerTempTable("breweries_tab")

    val country_breweries_cnt = spark.sqlContext.sql("SELECT country, COUNT(*) AS breweries_total FROM breweries_tab GROUP BY country")

    country_breweries_cnt.write.option("header", "true").csv("hdfs://172.17.0.5:9000/semestral_project/country_breweries_cnt.csv")

    val country_breweries_cnt_loaded = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load("hdfs://172.17.0.5:9000/semestral_project/country_breweries_cnt.csv")

    country_breweries_cnt.printSchema
    country_breweries_cnt_loaded.printSchema

    country_breweries_cnt.count()
    country_breweries_cnt_loaded.count()

    country_breweries_cnt.show()
    country_breweries_cnt_loaded.show()

#### Agragace 2

    val beers = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load("hdfs://172.17.0.5:9000/semestral_project/beers.csv")
    val reviews = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load("hdfs://172.17.0.5:9000/semestral_project/reviews.csv")


    beers.registerTempTable("beers_tab")
    reviews.registerTempTable("reviews_tab")


    beers.printSchema
    spark.sqlContext.sql("SELECT * FROM beers_tab").printSchema

    beers.count()
    spark.sqlContext.sql("SELECT * FROM beers_tab").count()

    beers.show()
    spark.sqlContext.sql("SELECT * FROM beers_tab").show()

    reviews.printSchema
    spark.sqlContext.sql("SELECT * FROM reviews_tab").printSchema

    reviews.count()
    spark.sqlContext.sql("SELECT * FROM reviews_tab").count()

    reviews.show()
    spark.sqlContext.sql("SELECT * FROM reviews_tab").show()


    val beers_average_score = spark.sqlContext.sql("SELECT FIRST(r.beer_id) AS beer_id, FIRST(b.name) AS name, FIRST(b.brewery_id) AS brewery_id, COUNT(r.beer_id) AS reviews_cnt, ROUND(AVG(r.score), 2) AS score_avg FROM reviews_tab r JOIN beers_tab b ON r.beer_id = b.id GROUP BY r.beer_id")

    beers_average_score.write.option("header", "true").csv("hdfs://172.17.0.5:9000/semestral_project/beers_average_score.csv")

    val beers_average_score_loaded = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load("hdfs://172.17.0.5:9000/semestral_project/beers_average_score.csv")

    beers_average_score.printSchema
    beers_average_score_loaded.printSchema

    beers_average_score.count()
    beers_average_score_loaded.count()

    beers_average_score.show()
    beers_average_score_loaded.show()

### Agragace 3    

    beers_average_score.registerTempTable("beers_average_score_tab")

    val breweries_overview = spark.sqlContext.sql("SELECT FIRST(b.name) AS brewery, COUNT(a.beer_id) AS beers_total, ROUND(AVG(a.score_avg), 2) AS average_rating FROM breweries_tab b JOIN beers_average_score_tab a ON b.id = a.brewery_id GROUP BY b.id")

    breweries_overview.write.option("header", "true").csv("hdfs://172.17.0.5:9000/semestral_project/breweries_overview.csv")

    val breweries_overview_loaded = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load("hdfs://172.17.0.5:9000/semestral_project/breweries_overview.csv")

    breweries_overview.printSchema
    breweries_overview_loaded.printSchema

    breweries_overview.count()
    breweries_overview_loaded.count()

    breweries_overview.show()
    breweries_overview_loaded.show()

## ElasticSearch a Kibana

Ve druhé části vytvoříme nad zvoleným datasetem index v ElasticSearch. Do tohoto indexu provedeme několik dotazů a nad datasetem vytvoříme vizualizace pomocí Kibany, které vyexportujeme.

### Logstash + ElasticSearch: Import dat a vytvoření indexu

Pro import dat do ElasticSearch využijeme nástroj LogStash.

Zkopírujte archiv s datasety do adresáře se vstupními datasety pro LogStash. Index vytvoříme nad datasetem beers.csv, tudíž ostatní datasety můžeme vymazat.

Pracujeme z rootovského adresáře projektu.

    cp datasets/datasets.tar.gz elastic/logstash/datasets/

    tar -xvf /elastic/logstash/datasets/datasets.tar.gz

    rm elastic/logstash/datasets/breweries.csv

    rm elastic/logstash/datasets/reviews.csv

Nyní máme připravená data pro importování do ElasticSearch.

V souboru elastic/logstash/config/pipelines.yml můžeme nalézt definici pipeliny pro import datasetu. Definice obsahuje název pipeliny a cestu k ní v rámci LogStash containeru, který brzy spustíme.

V souboru elastic/logstash/pipeline/logstash.conf můžeme nalézt samotnou definici pipeliny.

Nyní můžeme spustit Docker containery pro ElasticSearch, LogStash a Kibanu (na pozadí).

    cd elastic/

    sudo docker-compose up -d


Spustí se tři kontejnery. Pro kontrolu lze využít:

    sudo docker container ls

Pro získání stavu importu dat do Elasticu použitejte:

    docker logs -f logstash

### ElasticSearch + Kibana: Dotazy do indexu

Nyní můžeme přejít na dotazy směrem do indexu.

V Kibaně vytvoříme Index Pattern založený na již vytvořeném indexu v ElasticSearch.

Rozhraní Kibany je dostupné na adrese:

    localhost:5601

Vytvoření Index Pattern:
-   Panel Management
-   Index Patterns
-   Create Index Pattern
-   Next Step a vybrat, že chceme používat časový filter na základě sloupce @timestamp
-   Create Index Pattern

Měl by se objevit seznam definovaných sloupců.

Nyní můžeme přejít k dotazům do indexu.

Přepněte se do panelu Dev Tools.

#### Dotaz 1

Výběr piv v rozmezí 25 až 40 % obsahu alkoholu. Řazení sestupně dle obsahu alkoholu.

```
GET beers/_search
{
  "query": {
    "range": {
      "abv": {
        "gte": 25,
        "lte": 40
      }
    }
  },
  "sort": {
    "abv": {"order": "desc"}
  },
  "size": 50
}
```

#### Dotaz 2

Výběr piv s českým původem. Řezení vzestupně dle názvu.

```
GET beers/_search
{
  "query": {
    "match": {
      "country": "CZ"
    }
  },
  "sort": {
    "name.keyword": {"order": "asc"}
  }
}
```

#### Dotaz 3

Výběr piv typu Pilsner.

```
GET beers/_search
{
  "query": {
    "wildcard": {
      "style": "*pilsner*"
    }
  }
}
```

### Kibana: Vizualizace

V závěru přejdeme k vizualizacím nad datasetem.

V Kibaně:
-   Panel Management
-   Saved Object
-   Import a vybereme soubor elastic/dashboard.json
-   Potvrdíme dotaz na přepsání všech objektů, v dalším okně potvrdíme změny
-   Objeví se importovaný dashboard
-   Pozor na časový filtr (vpravo nahoře)

## Závěr
